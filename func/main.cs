using static System.Math;
using System;
using static System.Console;
using static math;

static class main
{

    
    const double inf = System.Double.PositiveInfinity;
    static void Main(){
        //Part A
        PartA();

        //Part B
        PartB();

        //Part C
        PartC();
    }

    static void PartA(){

        int p=0;
        
        Func<double,double> f1 = x => Log(x)/Sqrt(x);
        Func<double,double> f2 = x => Exp(-Pow(x,2));
        Func<double,double> f3 = (x) => Pow(Log(1/x),p);
    
        double[] a = {0,-inf,0};
        double[] b = {1, inf,1};

        WriteLine($"int_{a[0]}^{b[0]} (ln(x)/sqrt(x)) = {quad.o8av(f1,a[0],b[0])}");
        WriteLine($"int_{a[1]}^{b[1]} (Exp(-x^2)) = {quad.o8av(f2,a[1],b[1])}");
        for (int i = 0; i < 8; i++)
        {
            p=i;
            WriteLine($"int_{a[2]}^{b[2]} (ln(1/x)^{p}) = {quad.o8av(f3,a[2],b[2])}  \t gamma({p+1}) = {gamma(p+1)}");
        }
        
    }
    static void PartB(){
        Func<double,double> f1 = x => Sqrt(x)*Exp(-x);
        Func<double,double> f2 = x => Pow(x,3)/(Exp(x)-1);

        double[] a = {0,0,0};
        double[] b = {inf, inf,1};

        WriteLine($"int_{a[0]}^{b[0]} (sqrt(x)*exp(-x)) = {quad.o8av(f1,a[0],b[0])}");
        WriteLine($"int_{a[1]}^{b[1]} (x^3/exp(x)-1) = {quad.o8av(f2,a[1],b[1])}");
    }

    static void PartC(){
        WriteLine("===============================================================");
        WriteLine($"E(alfa)");
        double alfa;
        for (int i = 0; i < 100; i++)
        {
            alfa = i/33.0;
            Func<double,double> hamiltonian = x => ((-Pow(alfa,2)*Pow(x,2)/2) +alfa/2 + Pow(x,2)/2)*Exp(-alfa*Pow(x,2));
            Func<double,double> norm = x => Exp(-alfa*Pow(x,2));
            double E = quad.o8av(hamiltonian,-inf,inf)/quad.o8av(norm,-inf,inf);
            
            WriteLine($"{alfa} \t {E}");


        }
    }
}