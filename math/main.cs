using static System.Math;using static System.Console;using System;using static cmath;using static complex;


class main{


	static void Main(){
		
		complex I = complex.I;

		//Part A
		WriteLine($"sqrt(2) = {Sqrt(2)}");
		WriteLine($"exp(i) = {exp(I)}");
		WriteLine($"exp(i*Pi) = {exp(new complex(0,PI))}");
		WriteLine($"i^i = {cmath.pow(I,I)}");
		WriteLine("sin({0}*i)={1:f2}",PI,sin(new complex(0,PI)));

		//Part B
		WriteLine($"sinh(i) = {sinh(I)}");
		WriteLine($"cosh(i) = {cosh(I)}");	
		WriteLine($"sqrt(-1) = {sqrt(-1)}");
		WriteLine($"sqrt(i) = {sqrt(I)}");
	}
}

