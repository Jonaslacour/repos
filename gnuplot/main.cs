using static System.Console;
using static System.Math;


class main
{
    static void Main(){
        for (int i = -100; i < 100; i++)
        {   
            double x = i/20.0;
            WriteLine("{0} \t {1} \t {2}",(x),math.gamma(x), math.lngamma(x));
        }
        
        
    }
}