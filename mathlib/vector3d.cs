

public class vector3d : ivector3d{
	public int size{
		get{return data.Length;}
	}
	double[] data;
	public double this[int i]{
		get{return data[i];}
		set{data[i]=value;}
	}

	public double x{get{return this[0];}set{this[0]=value;}}
	public double y{get{return this[1];}set{this[1]=value;}}
	public double z{get{return this[2];}set{this[2]=value;}}
	

	public vector3d(){data = new double[] {0, 0, 0};}
	public vector3d(int n){data = new double[] {n, n, n};}
	public vector3d(double[] n){data = n;}
	public vector3d(int x, int y, int z){data = new double[]{x, y, z};}

	public void print(string s=""){
		System.Console.Write(s);
		for(int i=0; i<3; i++){
			System.Console.Write("{0:f1} \t", this[i]);
		}
		System.Console.WriteLine();
	} 

	
	//Interfaces in this case is bad because static implementations are not allowed
	public double DotProduct(vector3d v1, vector3d v2){
		double result = 0;
		
		for (int i=0;i<3; i++){result += v1[i]*v2[i];}

		return result;
	}

	public vector3d VectorProduct(vector3d v1, vector3d v2){
		vector3d result = new vector3d();
		result[0] = v1[1]*v2[2] - v1[2]*v2[1];
		result[1] = v1[2]*v2[0] - v1[0]*v2[2];
		result[2] = v1[0]*v2[1] - v1[1]*v2[0];
		return result;
	}


	public static vector3d operator+(vector3d u, vector3d v){
		vector3d result = new vector3d(u.size);

		for(int i=0;i<v.size;i++){
			result[i]=u[i]+v[i];
		}

		return result;
	}

	public static vector3d operator*(double a, vector3d v){
		vector3d result = new vector3d(v.size);

		for(int i=0;i<v.size;i++){
			result[i]=a*v[i];
		}

		return result;
	}

	public static vector3d operator*(vector3d v, double a){
		return a*v;
	}

	
}	