using System;

class Cmdline
{
    static void Main(string[] args)
    {
        foreach (var s in args)

        {
            double x;
            bool condition = double.TryParse(s,out x);
            if (condition){Console.WriteLine("{0} {1} {2}", x, Math.Sin(x), Math.Cos(x));}
        }
    }
}