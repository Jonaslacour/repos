using static System.Console;


class main{
	static void Main(){
		int n=3;
		vector3d v=new vector3d(n);
		vector3d u=new vector3d(n);

		for(int i=0; i<n;i++){
			v[i]=i;
			u[i]=-2*i;
		}


		v.print("v=");
		v[0]=5;
		v.print("v=");


		vector3d w=u+v;
		w.print("w=");

		vector3d v2 = v*2;

		v2.print("2*v=");

		//Interfaces in this case are bad because static implementations are not allowed thus the method has to be called on an instance of the class.
		double vw = v.DotProduct(v,w);

		vector3d v_cross_w = v.VectorProduct(v,w);
		
		WriteLine(vw);

		v_cross_w.print("v x W =");

		(v.VectorProduct(new vector3d(0,0,1),new vector3d(0,1,0))).print("i x j=");
		
		
	}
}