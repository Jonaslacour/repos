interface ivector3d{
    double DotProduct(vector3d a, vector3d b);
    vector3d VectorProduct(vector3d a, vector3d b);
}