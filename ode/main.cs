using System; using static System.Console; using System.Collections; using System.Collections.Generic;

class main
{
    static void Main(){
        //Part A
        PartA();

        //Part B
        PartB(new vector(1.0,0.0));
        PartB(new vector(1,-0.5));
        PartB(new vector(1,-0.5),0.01);
        WriteLine("test");
    }
    static void PartA(){
        Func<double, vector, vector> d1 = (x,y) => new vector(y[0]*(1-y[0]),0);
        double a = 0;
        double b = 3;
        List<double> X = new List<double>();
        List<vector> Y = new List<vector>();
        vector ya = new vector(0.5,0);
        vector yb = ode.rk23(d1,a,ya,b,X,Y);
        for (int i=0; i<X.Count; i++)
        {
            WriteLine($"{X[i]} \t {Y[i][0]}");
        }
    }

    static void PartB(vector ya, double eps=0, double a=0.0, double b=10.0){
        WriteLine($"Solving for ya = {ya[0]},{ya[1]}, eps = {eps}");
        Func<double, vector, vector> d1 = (x,y) => new vector(y[1],(1-y[0])+eps*y[0]*y[0]);
        List<double> X = new List<double>();
        List<vector> Y = new List<vector>();
        ode.rk23(d1, a, ya, b, X, Y);
        for (int i=0; i<X.Count; i++)
        {
            WriteLine($"{X[i]} \t {Y[i][0]} \t {Y[i][0]}");
        }
    }
}